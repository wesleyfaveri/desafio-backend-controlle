import express from 'express';
import formidableMiddleware from 'express-formidable';
import attachments from './attachments';

const app = express();

app.use(formidableMiddleware());

app.use(attachments);

export default app;
