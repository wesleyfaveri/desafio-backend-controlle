import express from 'express';
import { post, get } from './attachments';

const app = express();

const router = express.Router();

router.post('/attachments', post);
router.get('/attachments', get);

app.use('/', router);

export default app;
